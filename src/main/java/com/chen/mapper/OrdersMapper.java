package com.chen.mapper;

import com.chen.pojo.Orders;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单表 Mapper 接口
 * </p>
 *
 * @author whyme-chen
 * @since 2022-04-27
 */
public interface OrdersMapper extends BaseMapper<Orders> {

}
