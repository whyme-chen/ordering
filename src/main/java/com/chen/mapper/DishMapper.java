package com.chen.mapper;

import com.chen.pojo.Dish;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 菜品管理 Mapper 接口
 * </p>
 *
 * @author whyme-chen
 * @since 2022-04-27
 */
public interface DishMapper extends BaseMapper<Dish> {

}
