package com.chen.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.chen.common.Result;
import com.chen.dto.DishDto;
import com.chen.dto.SetmealDto;
import com.chen.pojo.Category;
import com.chen.pojo.Setmeal;
import com.chen.service.impl.CategoryServiceImpl;
import com.chen.service.impl.SetmealServiceImpl;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 套餐 前端控制器
 * </p>
 *
 * @author whyme-chen
 * @since 2022-04-27
 */
@Slf4j
@RestController
@RequestMapping("/setmeal")
public class SetmealController {

    @Autowired
    SetmealServiceImpl setmealService;

    @Autowired
    CategoryServiceImpl categoryService;

    @PostMapping
    @ApiOperation("新增套餐")
    public Result<String> addSetmeal(@RequestBody SetmealDto setmealDto){
        log.info("套餐信息：{}",setmealDto);
        log.info(setmealDto.toString());
        setmealService.saveWithDish(setmealDto);
        return Result.success("新增套餐成功！");
    }

    /**
     *删除套餐（单个或者批量）
     * @param ids
     * @return
     */
    @DeleteMapping
    @ApiOperation("删除套餐")
    public Result<String> deleteSetmeal(@RequestParam List<Long> ids){
        log.info("ids:{}",ids);
        setmealService.removeWithDish(ids);
        return Result.success("套餐删除成功！");
    }


    public Result<String> updateStatus(List<Long> ids){
        return Result.success("修改套餐状态成功");
    }

    /**
     * 分页查询套餐及套餐名筛选
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @GetMapping("/page")
    @ApiOperation("分页查询套餐")
    public Result<Page> queryPage(Integer page, Integer pageSize, String name){
        log.info("page:{},pageSize:{},name:{}",page,pageSize,name);
        Page<Setmeal> setmealPage = new Page<>(page,pageSize);
        LambdaQueryWrapper<Setmeal> setmealLambdaQueryWrapper = new LambdaQueryWrapper<>();
        setmealLambdaQueryWrapper.like(name!=null,Setmeal::getName,name);
        setmealService.page(setmealPage,setmealLambdaQueryWrapper);

        /**
         * 将套餐对应的套餐分类进行封装
         */
        Page<SetmealDto> setmealDtoPage = new Page<>();

        BeanUtils.copyProperties(setmealPage,setmealDtoPage,"records");
        setmealDtoPage.setRecords(setmealPage.getRecords().stream().map((item)->{
            SetmealDto setmealDto = new SetmealDto();
            BeanUtils.copyProperties(item,setmealDto);
            Category category = categoryService.getById(item.getCategoryId());
            setmealDto.setCategoryName(category.getName());
            return setmealDto;
        }).collect(Collectors.toList()));
        return Result.success(setmealDtoPage);
    }
}
