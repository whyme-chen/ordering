package com.chen.controller;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

/**
 * <p>
 * 地址管理 前端控制器
 * </p>
 *
 * @author whyme-chen
 * @since 2022-04-27
 */
@Api
@Controller
@RequestMapping("/addressBook")
public class AddressBookController {

}
