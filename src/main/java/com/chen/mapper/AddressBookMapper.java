package com.chen.mapper;

import com.chen.pojo.AddressBook;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 地址管理 Mapper 接口
 * </p>
 *
 * @author whyme-chen
 * @since 2022-04-27
 */
public interface AddressBookMapper extends BaseMapper<AddressBook> {

}
