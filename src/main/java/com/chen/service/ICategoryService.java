package com.chen.service;

import com.chen.pojo.Category;
import com.baomidou.mybatisplus.extension.service.IService;

import java.io.Serializable;

/**
 * <p>
 * 菜品及套餐分类 服务类
 * </p>
 *
 * @author whyme-chen
 * @since 2022-04-27
 */
public interface ICategoryService extends IService<Category> {

    boolean remove(Long id);

}
