package com.chen.service.impl;

import com.chen.pojo.SetmealDish;
import com.chen.mapper.SetmealDishMapper;
import com.chen.service.ISetmealDishService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 套餐菜品关系 服务实现类
 * </p>
 *
 * @author whyme-chen
 * @since 2022-04-27
 */
@Service
public class SetmealDishServiceImpl extends ServiceImpl<SetmealDishMapper, SetmealDish> implements ISetmealDishService {

}
