package com.chen.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.chen.common.CustomException;
import com.chen.pojo.Category;
import com.chen.mapper.CategoryMapper;
import com.chen.pojo.Dish;
import com.chen.pojo.Setmeal;
import com.chen.service.ICategoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 菜品及套餐分类 服务实现类
 * </p>
 *
 * @author whyme-chen
 * @since 2022-04-27
 */
@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements ICategoryService {

    @Autowired
    private CategoryMapper categoryMapper;

    @Autowired
    private DishServiceImpl dishService;

    @Autowired
    private SetmealServiceImpl setmealService;

    /**
     * 根据id删除分类
     * @param id
     * @return
     */
    @Override
    public boolean remove(Long id) {
        //检查该分类下是否有菜品
        LambdaQueryWrapper<Dish> dishLambdaQueryWrapper = new LambdaQueryWrapper<>();
        dishLambdaQueryWrapper.eq(Dish::getCategoryId,id);
        long count = dishService.count(dishLambdaQueryWrapper);
        if (count>0){//该分类下有菜品
            throw new CustomException("当前分类关联了菜品，不能删除！");
        }

        LambdaQueryWrapper<Setmeal> setmealLambdaQueryWrapper = new LambdaQueryWrapper<>();
        setmealLambdaQueryWrapper.eq(Setmeal::getCategoryId,id);
        long count1 = setmealService.count(setmealLambdaQueryWrapper);
        if (count1>0){//该套餐下有分类
            throw new CustomException("当前套餐关联了菜品，不能删除！");
        }

//        super.removeById(id);
        categoryMapper.deleteById(id);//删除该套餐
        return true;
    }
}
