package com.chen.service;

import com.chen.dto.SetmealDto;
import com.chen.pojo.Setmeal;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 套餐 服务类
 * </p>
 *
 * @author whyme-chen
 * @since 2022-04-27
 */
public interface ISetmealService extends IService<Setmeal> {

    void saveWithDish(SetmealDto setmealDto);

    void removeWithDish(List<Long> ids);
}
