package com.chen.config;

import com.chen.common.JacksonObjectMapper;
import com.chen.controller.interceptor.LoginInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**
 * @author whyme-chen
 * @date 2022/4/27 20:44
 */
@Slf4j
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    //    /**
//     * 设置静态资源映射
//     * @param registry
//     */
/*    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        log.info("静态资源映射配置");
        registry.addResourceHandler("/backend/**").addResourceLocations("classpath:/backend/");
        registry.addResourceHandler("/front/**").addResourceLocations("classpath:/front/");
    }*/

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        InterceptorRegistration registration = registry.addInterceptor(new LoginInterceptor());
        //拦截信息
        registration.addPathPatterns("/**");
        //不拦截信息
        registration.excludePathPatterns(
                "/employee/login",
                "/**/login.html",
                "/**/images/**/*.*",
                "/**/*.js",
                "/**/styles/**"
//            "/**/*.css",
//            "/**/images/*.*"
        );

    }

    /**
     * 扩展信息转换器
     * @param converters
     */
    @Override
    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        log.info("扩展消息转换器");
        //创建消息转换对象
        MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
        //设置具体对象映射器
        mappingJackson2HttpMessageConverter.setObjectMapper(new JacksonObjectMapper());
        //设置索引，将自定义转换器放置在首位，
        converters.add(0,mappingJackson2HttpMessageConverter);
    }
}
