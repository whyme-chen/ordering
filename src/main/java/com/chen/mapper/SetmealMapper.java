package com.chen.mapper;

import com.chen.pojo.Setmeal;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 套餐 Mapper 接口
 * </p>
 *
 * @author whyme-chen
 * @since 2022-04-27
 */
public interface SetmealMapper extends BaseMapper<Setmeal> {

}
