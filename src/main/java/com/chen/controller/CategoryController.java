package com.chen.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.chen.common.Result;
import com.chen.pojo.Category;
import com.chen.service.ICategoryService;
import com.chen.service.impl.CategoryServiceImpl;
import com.chen.service.impl.DishServiceImpl;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;

import java.util.List;

/**
 * <p>
 * 菜品及套餐分类 前端控制器
 * </p>
 *
 * @author whyme-chen
 * @since 2022-04-27
 */
@Slf4j
@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    CategoryServiceImpl categoryService;

    /**
     *新增分类
     * @param category
     * @return
     */
    @PostMapping
    @ApiOperation("新增分类")
    public Result<String> addCategory(@RequestBody Category category){
        log.info("category：{}",category.toString());
        categoryService.save(category);
        return Result.success("新增分类成功");
    }

    /**
     * 删除套餐或菜品分类
     * @param id
     * @return
     */
    @DeleteMapping
    @ApiOperation("删除套餐或菜品分类")
    public Result<String> deleteCategory(Long id){
        log.info("删除菜品的id：{}",id);
        categoryService.remove(id);
        return Result.success("分类信息删除成功！");
    }

    /**
     *修改菜品或套餐信息
     * @param category
     * @return
     */
    @PutMapping
    @ApiOperation("修改菜品或套餐信息")
    public Result<String> updateCategory(@RequestBody Category category){
        log.info("修改菜品或套餐的信息：{}",category);
        categoryService.updateById(category);
        return Result.success("修改成功！");
    }

    /**
     * 分页查询菜品和套餐
     * @param page
     * @param pageSize
     * @return
     */
    @GetMapping("/page")
    @ApiOperation("分页查询菜品和套餐")
    public Result<Page> queryPage(Integer page,Integer pageSize){
        log.info("page={},pageSize={}",page,pageSize);

        Page<Category> categoryPage = new Page<>();

        LambdaQueryWrapper<Category> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.orderByAsc(Category::getSort);

        categoryService.page(categoryPage,queryWrapper);
        return Result.success(categoryPage);
    }

    @GetMapping("/list")
    @ApiOperation("查询所有菜品分类")
    public Result<List<Category>> queryList(Category category){
        LambdaQueryWrapper<Category> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Category::getType,category.getType());
        queryWrapper.orderByAsc(Category::getSort).orderByDesc(Category::getUpdateTime);
        List<Category> list = categoryService.list(queryWrapper);
        return Result.success(list);
    }

}
