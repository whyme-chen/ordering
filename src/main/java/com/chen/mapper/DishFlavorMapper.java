package com.chen.mapper;

import com.chen.pojo.DishFlavor;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 菜品口味关系表 Mapper 接口
 * </p>
 *
 * @author whyme-chen
 * @since 2022-04-27
 */
public interface DishFlavorMapper extends BaseMapper<DishFlavor> {

}
