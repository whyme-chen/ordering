package com.chen.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

/**
 * <p>
 * 订单表 前端控制器
 * </p>
 *
 * @author whyme-chen
 * @since 2022-04-27
 */
@Controller
@RequestMapping("/orders")
public class OrdersController {

}
