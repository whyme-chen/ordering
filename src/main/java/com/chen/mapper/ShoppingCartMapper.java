package com.chen.mapper;

import com.chen.pojo.ShoppingCart;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 购物车 Mapper 接口
 * </p>
 *
 * @author whyme-chen
 * @since 2022-04-27
 */
public interface ShoppingCartMapper extends BaseMapper<ShoppingCart> {

}
