package com.chen.mapper;

import com.chen.pojo.Category;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 菜品及套餐分类 Mapper 接口
 * </p>
 *
 * @author whyme-chen
 * @since 2022-04-27
 */

@Repository
public interface CategoryMapper extends BaseMapper<Category> {

}
