package com.chen.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

/**
 * <p>
 * 套餐菜品关系 前端控制器
 * </p>
 *
 * @author whyme-chen
 * @since 2022-04-27
 */
@Controller
@RequestMapping("/setmealDish")
public class SetmealDishController {

}
