package com.chen.service;

import com.chen.pojo.Employee;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 员工信息 服务类
 * </p>
 *
 * @author whyme-chen
 * @since 2022-04-27
 */
public interface IEmployeeService extends IService<Employee> {

}
