package com.chen.service;

import com.chen.pojo.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户信息 服务类
 * </p>
 *
 * @author whyme-chen
 * @since 2022-04-27
 */
public interface IUserService extends IService<User> {

}
