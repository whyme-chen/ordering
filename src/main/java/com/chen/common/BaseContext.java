package com.chen.common;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 基于ThreadLocal封装的工具类，用于保存和获取当前登录用户id
 * @author whyme-chen
 * @date 2022/5/5 12:52
 */
@Slf4j
public class BaseContext {

    private static ThreadLocal<Long> threadLocal=new ThreadLocal<>();

    public static Long getCurrentId() {
        return threadLocal.get();
    }

    public static void setCurrentId(Long id) {
        log.info("当前线程中用户id为：{}",id);
        threadLocal.set(id);
    }
}
