package com.chen.controller;

import com.chen.common.Result;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.UUID;

/**
 * 主要用于文件上传和下载
 * @author whyme-chen
 * @date 2022/5/5 21:12
 */
@Slf4j
@RestController
@RequestMapping("/common")
public class CommonController {

    @Value("${ordering.uploadPath}")
    private String uploadPath;

    /**
     * 文件上传
     * @param file
     * @return
     */
    @PostMapping("/upload")
    @ApiOperation("文件上传")
    public Result<String> upload(MultipartFile file){
        log.info("{}",file);
        //转存文件
        //为防止文件名相同导致覆盖，使用uuid重新生成文件名
        String originalFilename = file.getOriginalFilename();
        //获得文件后缀名
        String fileSuffixName = originalFilename.substring(originalFilename.lastIndexOf("."));
        String fileName = UUID.randomUUID().toString()+fileSuffixName;

        //判断文件存放目录是否存在
        File dir = new File(uploadPath);
        if(!dir.exists()){
            dir.mkdirs();
        }
        try {

//            file.transferTo(new File(uploadPath+file.getOriginalFilename()));
            file.transferTo(new File(uploadPath+fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Result.success(fileName);
    }

    /**
     * 文件下载
     * @param name
     * @param response
     */
    @GetMapping("/download")
    @ApiOperation("文件下载")
    public void download(String name, HttpServletResponse response){
        FileInputStream fileInputStream;
        ServletOutputStream outputStream;
//        response.reset();
        try {
            fileInputStream= new FileInputStream(new File(uploadPath + name));
            outputStream= response.getOutputStream();
            response.setContentType("image/jpeg");
            int length=0;
            byte[] bytes = new byte[1024];
            while ((length=fileInputStream.read(bytes))!=-1){
                outputStream.write(bytes,0,length);
                outputStream.flush();
            }
            outputStream.close();
            fileInputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
