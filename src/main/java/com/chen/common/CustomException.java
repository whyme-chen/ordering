package com.chen.common;

/**
 * 自定义业务异常类
 * @author whyme-chen
 * @date 2022/5/5 20:31
 */
public class CustomException extends RuntimeException{

    public CustomException(String message) {
        super(message);
    }
}
