package com.chen.service;

import com.chen.pojo.Orders;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author whyme-chen
 * @since 2022-04-27
 */
public interface IOrdersService extends IService<Orders> {

}
