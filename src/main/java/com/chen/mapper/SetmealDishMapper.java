package com.chen.mapper;

import com.chen.pojo.SetmealDish;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 套餐菜品关系 Mapper 接口
 * </p>
 *
 * @author whyme-chen
 * @since 2022-04-27
 */
public interface SetmealDishMapper extends BaseMapper<SetmealDish> {

}
