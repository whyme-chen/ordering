package com.chen.mapper;

import com.chen.pojo.Employee;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 员工信息 Mapper 接口
 * </p>
 *
 * @author whyme-chen
 * @since 2022-04-27
 */
public interface EmployeeMapper extends BaseMapper<Employee> {

}
