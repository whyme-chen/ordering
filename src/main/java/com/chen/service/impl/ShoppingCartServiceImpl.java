package com.chen.service.impl;

import com.chen.pojo.ShoppingCart;
import com.chen.mapper.ShoppingCartMapper;
import com.chen.service.IShoppingCartService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 购物车 服务实现类
 * </p>
 *
 * @author whyme-chen
 * @since 2022-04-27
 */
@Service
public class ShoppingCartServiceImpl extends ServiceImpl<ShoppingCartMapper, ShoppingCart> implements IShoppingCartService {

}
