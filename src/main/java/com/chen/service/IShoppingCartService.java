package com.chen.service;

import com.chen.pojo.ShoppingCart;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 购物车 服务类
 * </p>
 *
 * @author whyme-chen
 * @since 2022-04-27
 */
public interface IShoppingCartService extends IService<ShoppingCart> {

}
