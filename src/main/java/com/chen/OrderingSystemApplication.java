package com.chen;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Slf4j
@SpringBootApplication
@MapperScan("com.chen.mapper")
@EnableTransactionManagement
public class OrderingSystemApplication {
    public static void main(String[] args) {
        SpringApplication.run(OrderingSystemApplication.class, args);
    }

}
