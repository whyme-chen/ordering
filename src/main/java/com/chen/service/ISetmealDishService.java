package com.chen.service;

import com.chen.pojo.SetmealDish;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 套餐菜品关系 服务类
 * </p>
 *
 * @author whyme-chen
 * @since 2022-04-27
 */
public interface ISetmealDishService extends IService<SetmealDish> {

}
