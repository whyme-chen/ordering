package com.chen.service;

import com.chen.pojo.OrderDetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单明细表 服务类
 * </p>
 *
 * @author whyme-chen
 * @since 2022-04-27
 */
public interface IOrderDetailService extends IService<OrderDetail> {

}
