package com.chen.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.chen.common.BaseContext;
import com.chen.common.Result;
import com.chen.pojo.Employee;
import com.chen.service.impl.EmployeeServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 员工信息 前端控制器
 * </p>
 *
 * @author whyme-chen
 * @since 2022-04-27
 */
@Slf4j
@Api(tags = "employee")
@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    EmployeeServiceImpl employeeService;

    /**
     * 员工登录校验
     * @param request
     * @param employee
     * @return
     */
    @PostMapping("/login")
    @ApiOperation("登录验证")
    public Result<Employee> login(HttpServletRequest request, @RequestBody Employee employee){
        String password = employee.getPassword();
        password = DigestUtils.md5DigestAsHex(password.getBytes());//将密码进行加密

        LambdaQueryWrapper<Employee> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Employee::getUsername,employee.getUsername());
        Employee emp = employeeService.getOne(queryWrapper);
        if (emp==null||!emp.getPassword().equals(password)){//账号和密码比对
            return Result.error("账号或密码错误！");
        }
        if (emp.getStatus()==0){//账号是否禁用
            return Result.error("抱歉，该账号已被禁用！");
        }

        //将员工id放入session
        request.getSession().setAttribute("employee",emp.getId());
        return Result.success(emp);
    }

    /**
     * 退出登录
     * @param request
     * @return
     */
    @PostMapping("/logout")
    @ApiOperation("退出登录")
    public Result<String> logout(HttpServletRequest request){
        request.getSession().removeAttribute("employee");
        return Result.success("退出成功！");
    }

    /**
     * 新增员工
     * @param request
     * @param employee
     * @return
     */
    @PostMapping
    @ApiOperation("新增员工")
    public Result<String> addEmployee(HttpServletRequest request,@RequestBody Employee employee){
        log.info("新增员工信息：{}",employee.toString());

        //设置默认密码、创建时间和更新时间
        employee.setPassword(DigestUtils.md5DigestAsHex("123456".getBytes()));
        //employee.setCreateTime(LocalDateTime.now());
        //employee.setUpdateTime(LocalDateTime.now());

        //获得创建用户
        //Long empId = (Long) request.getSession().getAttribute("employee");
        //employee.setCreateUser(empId);
        //employee.setUpdateUser(empId);

        employeeService.save(employee);
        return Result.success("新增成功");
    }


    /**
     * 更新员工信息
     * @param request
     * @param employee
     * @return
     */
    @PutMapping
    @ApiOperation("更新员工信息")
    public Result<String> updateEmployee(HttpServletRequest request,@RequestBody Employee employee){
        log.info("修改员工信息：{}",employee);

        //更新修改时间和修改人
        //employee.setUpdateTime(LocalDateTime.now());
        //employee.setUpdateUser((Long) request.getSession().getAttribute("employee"));

        employeeService.updateById(employee);
        return Result.success("更新成功！");
    }

    /**
     * 分页查询员工信息
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @GetMapping("/page")
    @ApiOperation("分页查找")
    public Result<Page> queryPage(Integer page,Integer pageSize,String name){
        log.info("page={}，pageSize={}，name={}",page,pageSize,name);

        //分页构造器
        Page pageInfo = new Page(page,pageSize);

        //条件构造器
        LambdaQueryWrapper<Employee> wrapper=new LambdaQueryWrapper<>();
        wrapper.like(StringUtils.isNotEmpty(name),Employee::getName,name);
        wrapper.orderByDesc(Employee::getUpdateTime);

        employeeService.page(pageInfo,wrapper);
        return Result.success(pageInfo);
    }

    /**
     * 根据id查询员工信息
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    @ApiOperation("根据id查询")
    public Result<Employee> getById(@PathVariable Long id){
        log.info("根据id查询");
        Employee employee = employeeService.getById(id);
        if(employee!=null){
            return Result.success(employee);
        }
        return Result.error("该成员不存在");
    }
}
