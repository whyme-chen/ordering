package com.chen.service;

import com.chen.pojo.AddressBook;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 地址管理 服务类
 * </p>
 *
 * @author whyme-chen
 * @since 2022-04-27
 */
public interface IAddressBookService extends IService<AddressBook> {

}
