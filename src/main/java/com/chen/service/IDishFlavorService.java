package com.chen.service;

import com.chen.pojo.DishFlavor;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 菜品口味关系表 服务类
 * </p>
 *
 * @author whyme-chen
 * @since 2022-04-27
 */
public interface IDishFlavorService extends IService<DishFlavor> {

}
