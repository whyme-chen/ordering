package com.chen;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collections;

@SpringBootTest
class OrderingSystemApplicationTests {

    @Test
    void contextLoads() {
    }

    /**
     * 代码生成器
     */
    @Test
    public void testGenerator(){
        FastAutoGenerator.create("jdbc:mysql://localhost:3306/ordering_system?serverTimezone=UTC",
                "root",
                "4112")
                .globalConfig(builder -> {
                    builder.author("whyme-chen") // 设置作者
                            .enableSwagger() // 开启 swagger 模式
                            .outputDir(System.getProperty("user.dir")+"/src/main/java/"); // 指定输出目录
                })
                .packageConfig(builder -> {
                    builder.parent("com.chen") // 设置父包名
                            .entity("pojo")
                            .pathInfo(Collections.singletonMap(OutputFile.xml, "resources/com/example/mapper")); // 设置mapperXml生成路径
                })
                .strategyConfig(builder -> {
                    builder.addInclude("address_book","category","dish","dish_flavor",
                            "employee","order_detail","orders","setmeal","setmeal_dish",
                            "shopping_cart","user"); // 设置需要生成的表名
//                            .addTablePrefix("sys_"); // 设置过滤表前缀
                })
                .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();

    }

}
