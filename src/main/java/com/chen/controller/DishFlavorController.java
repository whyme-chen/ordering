package com.chen.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 菜品口味关系表 前端控制器
 * </p>
 *
 * @author whyme-chen
 * @since 2022-04-27
 */
@Slf4j
@RestController
@RequestMapping("/dishFlavor")
public class DishFlavorController {

}
